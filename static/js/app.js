const choices = document.querySelectorAll('.choice');
const score = document.getElementById('score');
const result = document.getElementById('result');
const restart = document.getElementById('restart');
const modal = document.querySelector('.modal');
const scoreboard = {
    player: 0,
    computer: 0
}


// Play game
function play(event) {
    restart.style.display = 'inline-block';
    const playerChoice = event.target.id;
    const computerChoice = getComputerChoice();
    const winner = getWinner(playerChoice, computerChoice);
    showWinner(winner, computerChoice);
}

// Get computer choice
function getComputerChoice() {
    const rand = Math.random();
    if (rand < 0.34)
        return 'rock';
    else if (rand <= 0.67)
        return 'paper';
    else
        return 'scissors';
}

// Get winner
function getWinner(player, computer) {
    if (player === computer)
        return 'draw';
    else if (player === 'rock' && computer === 'scissors')
        return 'player';
    else if (player === 'paper' && computer === 'rock')
        return 'player';
    else if (player === 'scissors' && computer === 'paper')
        return 'player';
    else
        return 'computer';
}

function showWinner(winner, computer) {
    if (winner === 'player') {
        scoreboard.player += 1;
        result.innerHTML = `
            <h1 class="text-win">You Win!!!</h1>
            <i class="fas fa-hand-${computer}"></i>
            <p>Computer chose <strong>${computer}</strong></p>
            `;
    }
    else if (winner === 'computer') {
        scoreboard.computer += 1;
        result.innerHTML = `
            <h1 class="text-lose">You Lose!!!</h1>
            <i class="fas fa-hand-${computer}"></i>
            <p>Computer chose <strong>${computer}</strong></p>
            `;
    }
    else {
        result.innerHTML = `
            <h1>It's A Draw!!!</h1>
            <i class="fas fa-hand-${computer}"></i>
            <p>Computer chose <strong>${computer}</strong></p>
            `;
    }
    score.innerHTML = `
        <p>Player: ${scoreboard.player}</p>
        <p>Computer: ${scoreboard.computer}</p>
    `;
    modal.style.display = 'block';
    
}

// Clear modal
function clearModal(event) {
    if (event.target == modal)
        modal.style.display = 'none';
}

// Restart game
function restartGame(event) {
    scoreboard.player = 0;
    scoreboard.computer = 0;
    score.innerHTML = `
        <p>Player: 0</p>
        <p>Computer: 0</p>
    `;
}

// Event listeners
for (let choice of choices) 
    choice.addEventListener('click', play);

window.addEventListener('click', clearModal);
restart.addEventListener('click', restartGame);